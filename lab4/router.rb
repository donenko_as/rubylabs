module Resource
  def connection(routes)
    if routes.nil?
      puts "No route matches for #{self}"
      return
    end

    rescuePointer = false    

    begin

      loop do
        print 'Choose verb to interact with resources (GET/POST/PUT/DELETE) / q to exit: '
        verb = gets.chomp.upcase
        break if verb == 'Q'

        action = nil

        if verb == 'GET'
          print 'Choose action (index/show) / q to exit: '
          action = gets.chomp.downcase
          puts action.inspect
          if action != 'index' || action != 'show'
            rescuePointer = true
          end
          break if action == 'q'
        end

        action.nil? ? routes[verb].call : routes[verb][action].call
      end

    rescue
      puts 'There is no such method!'
      retry if rescuePointer == true
    end

  end
end

class CommentsController
  extend Resource
  NO_COMMENTS = 'There are no comments from this person!'

  def initialize
    @comments = {'Ivan': ['shit', 'good!', 'not good'], 'Sergey': ['very very bad', 'could be better']}
  end

  def index
    if @comments.empty? 
      puts 'There are no comments !'
    else
      @comments.each_pair do |author, comments| 
         puts "#{author}'s comments: "
         
         comments.each do |comment|
           puts "#{comments.index(comment)}) - '#{comment}'"
         end

         puts "\n"
      end 
    end
  end

  def show
    name = getAuthor
    comments = getAuthorComments(name)

    if comments != -1
       showAuthorsComments(name, comments)
    else
       puts NO_COMMENTS
    end
  end

  def create
    name = getAuthor
    comments = getAuthorComments(name)
    puts "Enter #{name}'s comment"
    comment = gets.chomp

    if comments != -1 
      comments.push(comment)
    else
      arr = [comment]
      @comments.store(:"#{name}", arr)
    end
  end

  def update
    name = getAuthor()
    comments = getAuthorComments(name)

    if comments != nil
       showAuthorsComments(name, comments)

       commentIndex = chooseFromComments(comments)

       if commentIndex != -1
          puts 'Enter a new comment'
          newComment = gets.chomp
          comments[commentIndex] = newComment
       else
          puts 'There is no comment with the entered number'
       end
    else
       puts NO_COMMENTS
    end
  end

  def chooseFromComments(comments)
    puts 'Enter the number to change (q to exit): '
    input = gets.to_i
    return comments[input] != '' ? input : -1 
  end

  def getAuthorComments(input)
    comments = @comments[:"#{input}"]
    return comments != nil ? comments : -1
  end

  def destroy
    puts 'Choose what to do (1 - delete single comment of the person, 2 - delete all comments of the person, 3 - delete ALL comments)'
    input = gets.chomp

    case input
    when '1'
      name = getAuthor
      comments = getAuthorComments(name)
      showAuthorsComments(name, comments)

      if comments != -1
        commentIndex = chooseFromComments(comments)
        comments.delete_at(commentIndex)
      else
        puts NO_COMMENTS
      end
    when '2'
      author = getAuthor()
      @comments.delete(:"#{author}")
    when '3'
      @comments.clear
    else
      puts 'Try another input!'
      destroy()
    end 
  end

  def getAuthor
    puts "Enter the name of the comment's author"
    return gets.chomp
  end

  def showAuthorsComments(name, comments)
    puts "#{name}'s comments are: "
 
    comments.each do |comment|
      puts "#{comments.index(comment)}) - '#{comment}'"
    end
  end

end 

class PostsController
  extend Resource
  NO_POST = 'There is no post with the entered input'

  def initialize
    @posts = []
  end

  def index
    if @posts.empty? 
      puts 'There are no posts !'
    else 
      size = @posts.length
      iterator = 0

      size.times do 
         print_pair(iterator)
         iterator += 1
      end
    end
  end

  def show
    index = check_if_post_exists()

    if index != -1 
      print_pair(index) 
    else 
      puts NO_POST
    end
  end

  def create
    puts 'Enter the text of the post: '
    input = gets.chomp
    index_new = 0
    if !@posts.empty?
      index_new = @posts.last.index + 1   
    end
    @posts.push(input)
    print_pair(index_new)
  end

  def update
    index = check_if_post_exists()

    if index != -1 
      puts 'Enter the new text of the post'
      new_text = gets.chomp
      @posts[index] = new_text
      print_pair(index)
    else
      puts NO_POST
    end
  end

  def destroy
    index = check_if_post_exists()
    
    if index != -1 
      @posts.delete_at(index)
    else 
      puts NO_POST
    end
  end

  def check_if_post_exists
    puts 'Enter the number of the post: '
    input = gets.to_i
    return @posts.include?(@posts[input]) ? input : -1
  end
  
  def print_pair(index)
    puts "[#{index}] - #{@posts[index]}"
  end
 
end

class Router
  def initialize
    @routes = {}
  end

  def init
    resources(PostsController, 'posts')
    resources(CommentsController, 'comments')

    loop do
      print 'Choose resource you want to interact (1 - Posts, 2 - Comments, q - Exit): '
      choise = gets.chomp

      PostsController.connection(@routes['posts']) if choise == '1'
      CommentsController.connection(@routes['comments']) if choise == '2'
      break if choise == 'q'
    end

    puts 'Good bye!'
  end

  def resources(klass, keyword)
    controller = klass.new
    @routes[keyword] = {
      'GET' => {
        'index' => controller.method(:index),
        'show' => controller.method(:show)
      },
      'POST' => controller.method(:create),
      'PUT' => controller.method(:update),
      'DELETE' => controller.method(:destroy)
    }
  end
end

router = Router.new

router.init
