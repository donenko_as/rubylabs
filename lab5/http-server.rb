require 'socket'
require 'rack'
require 'rack/utils'
require './cash_machine.rb'

server = TCPServer.new('0.0.0.0', 3000)

class App
	def call(env)
		req = Rack::Request.new(env)
    atm = CashMachine.new
    atm.init()
    value = req.query_string.to_f.round(2)

		case req.path
		when '/deposit'
			[200, {'Content-Type' => 'text/html'}, ["#{atm.deposit(value)}"]]
		when '/withdraw'
      [200, {'Content-Type' => 'text/html'}, ["#{atm.withdraw(value)}"]]
    when '/balance'
			[200, {'Content-Type' => 'text/html'}, ["#{atm.balance()}"]]
		else
			[404, {'Content-Type' => 'text/html'}, ["Not found"]]
		end
	end
end

app = App.new

while connection = server.accept
  request = connection.gets
  method, full_path = request.split(' ')
  path = full_path.split('?')[0]
  value = full_path.split('=')[1]
  
  status, headers, body = app.call({
    'REQUEST_METHOD' => method,
    'PATH_INFO' => path,
    'QUERY_STRING'=> value
  })
 
  connection.print("HTTP/1.1 #{status}\r\n")

  headers.each { |key, value|  connection.print("#{key}: #{value}\r\n") }

  connection.print "\r\n"

  body.each { |part| connection.print(part) }

  connection.close
end
