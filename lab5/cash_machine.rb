class CashMachine
  START_BALANCE = 100.0
  FILENAME = 'balance.txt'
 
  def initialize()

  end

  def init
    if !File.exists?(FILENAME)     
      File.write(FILENAME, START_BALANCE,  mode: 'a')
    end
  end

  def balance
    balance = get_from_file()
    return "Your balance is  #{balance}" 
  end
  
  def withdraw(value)
    balance = get_from_file()
    new_balance = balance - value.round(2)
    if value <= 0 
      return 'The entered sum cannot be negative or zero!'
    else 
      if new_balance < 0 
        return 'The sum you have entered exeeds accounts ones. Check your balance !'
      else
        override(new_balance)
        return "New balance is: #{new_balance}"
      end
    end   
  end

  def deposit(value)
    if value <= 0
      return 'Invalid sum entered! The sum cannot be lower or equal to zero!'
    else
      balance = get_from_file()
      balance += value.round(2)
      override(balance)
      return "Your new balance is : #{balance}"
    end  
  end
  
  def override(value)
    file = File.open(FILENAME, "w")
    file.write value  
    file.close
  end

  def get_from_file
    file = File.open(FILENAME)
    balance = File.read(file).to_f
    file.close
    return balance
  end

end


