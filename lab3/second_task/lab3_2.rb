class BankAccount
  START_BALANCE = 100.0
  FILENAME = 'balance.txt'
 
  def initialize
    @balance = nil
    @file = nil
  end

  def init
    if File.exists?(FILENAME)
        file = File.open(FILENAME)
        @balance = File.read(file).to_f
        puts @balance        
    else     
      File.write(file, START_BALANCE,  mode: 'a')
      @balance = START_BALANCE
    end
    @file = file
    action_select_menu()    
  end

  def print_balance
    puts @balance 
  end
  
  def get_from_balance
    puts 'Enter the sum to withdraw:'
    sum = gets.to_f
    temp = @balance - sum
    if sum <= 0 
      puts 'The entered sum cannot be negative or zero!'
    else 
      if temp < 0 
        puts 'The sum you have entered exeeds accounts ones. Check your balance !'
      else
        @balance = temp
        print_balance()
      end
    end   
  end

  def give_to_balance
    puts 'Enter the sum(greater than 0) to deposit:'
    sum = gets.to_f
    if sum <= 0
      puts 'invalid sum entered! The sum cannot be lower or equal to zero!'
    else
      @balance += sum
      print_balance()
    end  
  end
  
  def action_select_menu
    while true
      puts "\n" 
      puts 'To deposit money to the accout press "D(d)"'
      puts 'To withdraw money from the account pres "W(w)"'
      puts 'To check the balance press "B(b)"'
      puts 'To Quit press "Q(q)"'
      puts "\n"
      input = gets.chomp.upcase
      case input 
      when 'D' 
        give_to_balance()
      when 'W' 
        get_from_balance()
      when 'B' 
        print_balance() 
      when 'Q' 
        puts 'Quiting app...'
        File.write(@file, @balance)
        @file.close
        break
      else 
        puts 'wrong input!'    
      end
    end
  end
end

bankAccount = BankAccount.new
bankAccount.init

