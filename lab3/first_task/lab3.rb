def get_file_data
  file = 'input.txt'
  if File.exists?(file)
    file = File.open(file)
    file_data = File.read(file).split("\n")
    file.close
    return file_data
  else
    puts 'there is no such file !'
    return -1 
  end
end

def interface(output_files_path)
  file_data = get_file_data()
  until file_data.empty? do
    puts 'enter the age'
    entered_number = gets.to_i
    if entered_number == -1 
      puts 'you have entered -1'
      break
    else
      pointer_no_such_age = false
      num = entered_number.to_s
      file_data.length.times do 
        pointer_no_such_age = array_find_delete_write_to_file(file_data, num, output_files_path, pointer_no_such_age)
      end 
      if pointer_no_such_age == false
        puts 'there is no data with the entered age !'
      end
    end
  end
end

def array_find_delete_write_to_file(file_data, num, output_files_path, status)
  file_data.each do |string|    
    if string.include? num
      File.write(output_files_path +'result.txt', "#{string}\n",  mode: 'a')
      file_data.delete(string)
      return true   
    end
  end
  return status == true ? true : false
end  
   
interface('/home/mangos/rubylabs/lab3/first_task/')
