class DropAll < ActiveRecord::Migration[6.0]
  def change
    drop_table :lab_reports
  end
end
