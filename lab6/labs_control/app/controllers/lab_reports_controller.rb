class LabReportsController < ApplicationController
    
    def index
      @lab_report = LabReport.all
    end

    def new
      @lab_report = LabReport.new
    end

    def show
      @lab_report = LabReport.find(params[:id])
    end

    def create
      @lab_report = LabReport.new(lab_report_params)

      if (@lab_report.save)
        redirect_to @lab_report
      else
        render 'new'
      end
    end

    private def lab_report_params 
        params.require(:lab_report).permit(:title, :description)
    end

end
