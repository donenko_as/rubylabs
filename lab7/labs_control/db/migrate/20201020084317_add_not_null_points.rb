class AddNotNullPoints < ActiveRecord::Migration[6.0]
  def change
    change_column :marks, :points, :integer, :null => false
  end
end
