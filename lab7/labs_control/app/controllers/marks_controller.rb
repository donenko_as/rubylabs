class MarksController < ApplicationController
    before_action :get_report

    def create 
      get_report()
      mark_letter = get_mark()
      
      @mark = @lab_report.build_mark(points: mark_letter[:mark], letter: mark_letter[:letter])
      
      if(@mark.save) 
        redirect_to '/'
      else
        render 'new'
      end 
    end
    
    def destroy
      @lab_report = LabReport.find(params[:lab_report_id])
      
      @mark = @lab_report.mark.destroy()
      
      @mark.destroy

      redirect_to '/'
    end

    private def convert_to_letter(mark)
      case mark
      when 90..100
        return 'A'
      when 80..89
        return 'B'
      when 65..79
        return 'C'
      when 55..64
        return 'D'
      when 50..54
        return 'E'
      when 1..49
        return 'F'
      else
        return 'mark is nill'
      end
    end

    private def get_mark
      hash = params.require(:mark).permit(:points)
      
      if hash[:points] != ""
        mark_int = hash[:points].to_i
        
        mark_letter = convert_to_letter(mark_int)
      
        hash = {mark: mark_int, letter: mark_letter}
      else
        mark_int = nil

        mark_letter = nil
      end

      hash = {mark: mark_int, letter: mark_letter}

      return hash
    end

    private def get_report
      @lab_report = LabReport.find(params[:lab_report_id])
    end
end
