require 'rails_helper'

RSpec.describe MarksController, type: :controller do
    describe "#create" do
      before(:each) do 
        @report = LabReport.create(title: 'new', description: 'this')
        @report = LabReport.last
      end

      it "redirects to root page if save is succesful" do
        post :create, :params =>  { :lab_report_id => @report.id,  :mark => {:points => 90, :letter => 'A', }}
        
        expect(response).to redirect_to('/')
      end

      it "renders new if input is wrong" do
        post :create, :params =>  { :lab_report_id => @report.id,  :mark => {:points => 190, :letter => 'A', }}

        expect(response).to render_template('new') 
      end

    end

    describe "#destroy" do
      before(:each) do 
        @report = LabReport.create(title: 'new', description: 'this')
        @report = LabReport.last
        @mark = @report.create_mark(points: 90, letter: 'A')
      end

      it "destroys mark succesfuly and redirects to root" do
        expect{delete :destroy, :params => {:lab_report_id => @report.id, :id =>  @mark.id}}.to change{Mark.count}.by(-1)
      
        expect(response).to redirect_to('/')
      end
    end 
end
