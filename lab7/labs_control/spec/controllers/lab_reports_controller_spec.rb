require 'rails_helper'

RSpec.describe LabReportsController, type: :controller do

    describe "#create" do
      it "redirects to root page if save is succesful" do 
        post :create, :params => { :lab_report => { :title => 'title', :description => 'fdsfsdffsdfsdf' } } 

        expect(response).to redirect_to(lab_report_path(assigns(:lab_report)))
      end

      it "renders new if save is unsuccesful" do
        post :create, :params => { :lab_report => { :title => '', :description => 'fdsfsdffsdfsdf' } } 

        expect(response).to render_template('new') 
      end

    end

    describe "#index" do
      before(:each) do 
        get :index
      end
      
      it "is expected do return status 200" do 
        expect(response.status).to eq(200) 
      end

      it "is expected to render root page" do 
        response.should render_template('/')
      end

      it "assigns all reports to @lab_report" do 
        expect(assigns(:lab_report)).to eq(LabReport.all)
      end
    end

    describe "#show" do
      it "assign found report to @lab_report" do 
        @report = LabReport.create(title: 'new', description: 'this') 

        get :show, params: { id: @report.id }
        
        expect(assigns(:lab_report)).to eq(LabReport.find(@report.id))
      end
    end
end
