require 'rails_helper'

RSpec.describe Mark, type: :model do
  it "must be created succesfully" do
    report = LabReport.create(title: 'new', description: 'this')
    mark = report.build_mark(points: 90, letter: 'A')
    expect(mark).to be_valid 
  end

  it "must not be created without lab_report" do
    mark = Mark.create(points: 23)
    expect(mark).to_not be_valid 
  end

  it "must fail if points are nil" do
    mark = Mark.create()
    expect(mark).to_not be_valid 
  end

  it "must fail if points are lower than zero" do
    mark = Mark.create(points: -23)
    expect(mark).to_not be_valid  
  end

  it "must fail if points are greater than 100" do
    mark = Mark.create(points: 101)
    expect(mark).to_not be_valid 
  end
end
