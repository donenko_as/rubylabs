require 'rails_helper'

RSpec.describe LabReport, type: :model do

  describe "mark creating succesful and report has only one mark" do
    
    before(:each) do
      @report = LabReport.create(title: 'new', description: 'this')
    end
    
    it "must be created succesfully" do 
      expect(@report).to be_valid
    end
  
    it "must not have more than 1 mark" do 
      mark1 = @report.build_mark(points: 90, letter: 'A')
      mark2 = @report.build_mark(points: 20, letter: 'F')
      
      expect(@report.mark.points).to eq(20)
    end
  end

  it "is not valid without a title" do 
    report = LabReport.create(description: 'this')

    expect(report).to_not be_valid
  end

  it "is not valid without description" do
    report = LabReport.create(title: 'new')

    expect(report).to_not be_valid
  end

  it "must fail if title has more than 250 symbols" do
    title_to_test = ""
    251.times {title_to_test += 'a'} 
     
    report =LabReport.create(title: title_to_test, description: 'this')
    expect(report).to_not be_valid
  end

  it "must fail if descriprion has more than 500 symbols" do
    description_to_test = ""
    501.times {description_to_test += 'a'} 
     
    report =LabReport.create(title: 'bew', description: description_to_test)
    expect(report).to_not be_valid
  end

end
